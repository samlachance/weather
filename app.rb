require 'net/http'

get '/' do
  
end

get '/:zip' do
  api_key = ENV["OPEN_WEATHER_KEY"]
  uri = URI("http://api.openweathermap.org/data/2.5/weather?zip=#{params[:zip]}&APPID=#{api_key}&units=imperial")
  @resp = JSON.parse(Net::HTTP.get(uri))

  slim :index
end
