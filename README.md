## Weather app

This is a no-nonsense weather app.

I created this app because most weather sites/apps are bloated and sell your location data to advertisers.

Simply load the page with your zipcode as the param to receive a simple weather update.